FDMAP input file:
 rupture with slip-weakening friction
 caldera ring faulting study
 version with acoustic magma chamber

You can add comments to the input file, even in namelists.
In namelists, these must be preceded by a !

Some visualization tips:
X4=loadfast(A,'I4_x');Y4=loadfast(A,'I4_y');
X6=loadfast(A,'I6_x');Y6=loadfast(A,'I6_y');
V4=loadfast(A,'I4_V');V6=loadfast(A,'I6_V');
subplot(2,1,1),pcolored(X6,V6.t,V6.V),subplot(2,1,2),pcolored(X4,V4.t,V4.V)
x=loadfast(A,'B_x');y=loadfast(A,'B_y');
vx=loadfast(A,'B_vx');
for n=1:10:vx.nt,pcolored(y,x,vx.vx(:,:,n)),set(gca,'ydir','reverse'),axis image,drawnow,end

&problem_list
 name = 'data/caldera2', ! problem name (prefix to all files, including path)
 ninfo = 10,  ! print update on timing to stdout every ninfo time steps
 nt = 20001,   ! total number of time steps
 CFL = 0.5d0, ! CFL parameter (sets time step for stability)
 refine = 1d0 ! space/time refinement factor, increase for more accurate solution
 /

&domain_list
 mode = 2, ! 2 = plane strain, 3 = antiplane shear
 nx = 802, ny = 1603, ! number of grid points in global mesh
 nblocks = 6, ! number of blocks
 nblocks_x = 2, nblocks_y = 3, ! number of blocks in each direction
 nifaces = 7 ! number of interfaces
 /

The general block setup is as follows:

x = depth, increasing downward from free surface at x=0
y = horizontal

all blocks with uniform grid spacing h = 5 m
left set of blocks = elastic: 0 km < x < 2 km
right set of blocks = acoustic: 2 km < x < 4 km
==> 2 km / 5 m = 400 grid intervals ==> 401 grid points

faults at y = -0.5 km and y = +0.5 km

BLOCK5,6 (top)      -4 km < y < -0.5 km ==> 701 grid points
BLOCK3,4 (middle) -0.5 km < y <  0.5 km ==> 201 grid points
BLOCK1,2 (bottom)  0.5 km < y <    4 km ==> 701 grid points
total grid points in y direction = 701+201+701 = 1603

!---BLOCK1---

&grid_list
 nx = 401, ny = 701, ! number of grid points in this block
 iblock_x = 1, iblock_y = 1, ! block indices
 LT = 0d0,-0.5d0, ! (x,y) coordinates of left top corner
 RT = 2d0,-0.5d0, ! right top corner
 LB = 0d0,-4d0,  ! left bottom corner
 RB = 2d0,-4d0   ! right bottom corner
 /

&boundaries_list
 bcL = 'free', ! boundary condition on left side
 bcR = 'none', ! right side
 bcB = 'absorbing', ! bottom side
 bcT = 'none'       ! top side (set to none for interface)
 /

&material_list
 rho = 2.8d0, ! density
 cs = 1d0, ! S-wave speed
 cp = 1.73d0      ! P-wave speed
/

!---BLOCK2---

&grid_list
 nx = 401, ny = 701,
 iblock_x = 2, iblock_y = 1,
 LT = 2d0,-0.5d0, RT = 4d0,-0.5d0,
 LB = 2d0,  -4d0, RB = 4d0,  -4d0 /

&boundaries_list
 bcL = 'none', bcR = 'absorbing', bcB = 'absorbing', bcT = 'none' /

&material_list
 rho = 2.8d0, cs = 1d0, cp = 1.73d0 /

/

!---BLOCK3---

&grid_list
 nx = 401, ny = 201, iblock_x = 1, iblock_y = 2,
 LT = 0d0, 0.5d0, RT = 2d0, 0.5d0,
 LB = 0d0,-0.5d0, RB = 2d0,-0.5d0 /

&boundaries_list
 bcL = 'free', bcR = 'none', bcB = 'none', bcT = 'none' /

&material_list
 rho = 2.8d0, cs = 1d0, cp = 1.73d0 /

!---BLOCK4---

&grid_list
 nx = 401, ny = 201,
 iblock_x = 2, iblock_y = 2,
 LT = 2d0, 0.5d0, RT = 4d0, 0.5d0,
 LB = 2d0,-0.5d0, RB = 4d0,-0.5d0 /

&boundaries_list
 bcL = 'none', bcR = 'rigid', bcB = 'none', bcT = 'none' /

This is the "magma chamber" block, idealized as an acoustic medium.
Set S-wave speed cs to very small (but nonzero, in this code version) value.
Note also that right side BC is rigid, which will allow chamber to pressurize
when compressed. Better solution would be 9 block set up with 3 additional
elastic blocks at greater x values than in model here.

&material_list
 rho = 2.8d0, cs = 1d-6, cp = 1d0 /

!---BLOCK5---

&grid_list
 nx = 401, ny = 701, iblock_x = 1, iblock_y = 3,
 LT = 0d0,  4d0, RT = 2d0,  4d0,
 LB = 0d0,0.5d0, RB = 2d0,0.5d0 /

&boundaries_list
 bcL = 'free', bcR = 'none', bcB = 'none', bcT = 'absorbing' /

&material_list
 rho = 2.8d0, cs = 1d0, cp = 1.73d0 /

!---BLOCK6---

&grid_list
 nx = 401, ny = 701,
 iblock_x = 2, iblock_y = 3,
 LT = 2d0,  4d0, RT = 4d0,  4d0,
 LB = 2d0,0.5d0, RB = 4d0,0.5d0 /

&boundaries_list
 bcL = 'none', bcR = 'absorbing', bcB = 'none', bcT = 'absorbing' /

&material_list
 rho = 2.8d0, cs = 1d0, cp = 1.73d0 /

!---ALL BLOCKS---

spatially uniform prestress can be added here
or directly to fault (we'll do the latter for shear stress on the two faults)

&fields_list
 syy0 = -20d0, ! yy component of prestress (fault normal compression)
 sxy0 = 0d0, ! xy component of prestress (shear stress on fault)
 sxx0 = -20d0 ! xx component of prestress (fault parallel compression)
 /

&operator_list
 Cdiss = 1d0 / ! artificial dissipation to control numerical oscillations

!---IFACE1---

&interface_list
 coupling = 'locked', ! locked interface
 iblockm = 1, iblockp = 2 ! between blocks 1 and 2
/

!---IFACE2---

&interface_list
 coupling = 'locked', iblockm = 3, iblockp = 4 /

!---IFACE3---

&interface_list
 coupling = 'locked', iblockm = 5, iblockp = 6 /

!---IFACE4---

This is the bottom fault. Rupture will be initiated on top fault.

&interface_list
 coupling = 'friction', ! frictional interface
 iblockm = 1, iblockp = 3, ! between blocks 1 and 3
/

&friction_list
 friction_law = 'SW', ! slip-weakening friction
 sw%fs = 0.677d0, ! static friction
 sw%fd = 0.525d0, ! dynamic friction
 sw%Dc = 0.04d0,   ! slip-weakening distance
 ld%shape = 'uniform', ! uniform prestress
 ld%S = 11.7d0 ! shear prestress
 /

!---IFACE5---

&interface_list
 coupling = 'locked', iblockm = 2, iblockp = 4 /

!---IFACE6---

This is the top fault. Initiate rupture on this fault.

&interface_list
 coupling = 'friction', iblockm = 3, iblockp = 5 /

&friction_list
 friction_law = 'SW', ! slip-weakening friction
 sw%fs = 0.677d0, ! static friction
 sw%fd = 0.525d0, ! dynamic friction
 sw%Dc = 0.04d0,   ! slip-weakening distance
 ld%shape = 'uniform', ! uniform prestress
 ld%S = -11.7d0 ! shear prestress
 force = T, ! force rupture expansion to nucleate
 kn%fd = 0.525d0, ! dynamic friction (forced nucleation)
 kn%dfdx = 2d0, ! rate of increase of friction with x (forced nucleation)
 kn%vL = -0.3d0,    ! rupture velocity of left tip (forced nucleation)
 kn%vR =  0.3d0,    ! rupture velocity of right tip (forced nucleation)
 kn%xL = 0.576d0, ! initial position of left tip (forced nucleation)
 kn%xR = 0.424d0, ! initial position of right tip (forced nucleation)
 kn%tmax = 1d0    ! stop forcing expansion after this time
 /

Initiation centered at x = 0.5 km ==> xL = 0.5+0.076 = 0.576, xR = 0.5-0.076 = 0.424

!---IFACE7---

&interface_list
 coupling = 'locked', iblockm = 4, iblockp = 6 /

The output list below controls which fields will be output (written to
disk as binary data files) and how frequently. These are not namelists
and comments are not permitted. The list is organized into multiple
sections, such as
	  I
	  iface1
	  D V S N Psi
	  0d0 1d10 10
where "I" is an arbitrary name used to help you organize output, 
"iface1" is the location of the fields to be output, "D V S N"
is a list of the fields to be output (order does not matter), and
"0d0 1d10 1" saves data between times t=0 and t=10^10 (basically 
for all time) every 1 time steps.

!---begin:output_list---
I4
iface4
x y
0d0 0d0 1
I4
iface4
D V S N
0d0 1d10 10
I6
iface6
x y
0d0 0d0 1
I6
iface6
D V S N
0d0 1d10 10
B
body
x y
0d0 0d0 1
B
body
vx vy sxx sxy syy
0d0 1d10 500
!---end:output_list---

