% script illustrating how to enter spatially variable prestress

nx = 801; x = linspace(-40,40,nx)'; % coordinates along fault, column vector

% prestress (in addition to resolved stresses in body)
S0 = cos(2*pi*x/10); % shear stress
N0 = sin(2*pi*x/4);  % normal stress (positive in compression)

% write file
filename = 'gp229mode2.prestress'; % any name and extension is ok
fdmap_write_fault_prestress(filename,S0,N0);