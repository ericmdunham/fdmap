FDMAP input file:
 rupture with slip-weakening friction
 fault bend study

***INSTRUCTIONS ON CHANGING BEND ANGLE***
The domain is divided into four blocks, with all four blocks
meeting at the bend (x,y)=(20,0). To vary the bend angle,
you need to change the coordinates of two of the block corners
that coincide with the rightmost point of the fault. These
are the RT="right,top" point of block 2 and RB="right,bottom"
of block 4. These would both be (x,y)=(40,0) for a planar fault.
Currently they are (x,y)=(40,1.7498), which corresponds to a
bend angle of 5 deg. This comes from tan(theta)=y/(40-20)=y/20.
So to change the bend angle to -10 deg, set y=-3.5265. The two
places to make this change are indicated below by
         ! ***CHANGE BEND ANGLE HERE***

***INSTRUCTIONS FOR MATLAB VISUALIZATION***
Here are some Matlab commands to make relevant plots:
A = init('gp229bend','~/fdmap/data'); % change path to where you saved data
X2 = loadfast(A,'I2_x'); X3 = loadfast(A,'I3_x'); X = [X2; X3];
Y2 = loadfast(A,'I2_y'); Y3 = loadfast(A,'I3_y'); Y = [Y2; Y3];
plot(X,Y,'o'),axis equal % plot fault geometry

V2 = loadfast(A,'I2_V'); V3 = loadfast(A,'I3_V'); V = [V2.V; V3.V];
S2 = loadfast(A,'I2_S'); S3 = loadfast(A,'I3_S'); S = [S2.S; S3.S];
N2 = loadfast(A,'I2_N'); N3 = loadfast(A,'I3_N'); N = [N2.N; N3.N];
% V is slip velocity array for both fault segments
% S and N are shear and normal stress on both fault segments

plot(X,S(:,1)./N(:,1)) % initial shear/normal stress



You can add comments to the input file, even in namelists.
In namelists, these must be preceded by a !

&problem_list
 name = 'data/gp229bend', ! problem name (prefix to all files, including path)
 ninfo = 10,  ! print update on timing to stdout every ninfo time steps
 nt = 3001,   ! total number of time steps
 CFL = 0.5d0, ! CFL parameter (sets time step for stability)
 refine = 1d0 ! space/time refinement factor, increase for more accurate solution
 /

&domain_list
 mode = 2, ! 2 = plane strain, 3 = antiplane shear
 nx = 802, ny = 802, ! number of grid points in global mesh
 nblocks = 4, ! number of blocks
 nblocks_x = 2, nblocks_y = 2, ! number of blocks in each direction
 nifaces = 4 ! number of interfaces
 /

!---BLOCK1---

&grid_list
 nx = 601, ny = 401, ! number of grid points in this block
 mgx = 1, mgy = 1, ! global indices of left bottom corner of block
 iblock_x = 1, iblock_y = 1, ! block indices
 LT = -40d0,  0d0, ! (x,y) coordinates of left top corner
 RT =  20d0,  0d0, ! right top corner
 LB = -40d0,-40d0, ! left bottom corner
 RB =  20d0,-40d0  ! right bottom corner
 /

&boundaries_list
 bcL = 'absorbing', ! boundary condition on left side
 bcR = 'none', ! right side
 bcB = 'absorbing', ! bottom side
 bcT = 'none'       ! top side (set to none for interface)
 /

!---BLOCK2---

&grid_list
 nx = 201, ny = 401, mgx = 602, mgy = 1, iblock_x = 2, iblock_y = 1,
 LT = 20d0,  0d0, RT = 40d0, 1.7498d0, ! ***CHANGE BEND ANGLE HERE***
 LB = 20d0,-40d0, RB = 40d0,-40d0 /

&boundaries_list
 bcL = 'none', bcR = 'absorbing', bcB = 'absorbing', bcT = 'none' /

!---BLOCK3---

&grid_list
 nx = 601, ny = 401, mgx = 1, mgy = 402, iblock_x = 1, iblock_y = 2,
 LT = -40d0,40d0, RT = 20d0,40d0,
 LB = -40d0, 0d0, RB = 20d0, 0d0 /

&boundaries_list
 bcL = 'absorbing', bcR = 'none', bcB = 'none', bcT = 'absorbing' /

!---BLOCK4---

&grid_list
 nx = 201, ny = 401, mgx = 602, mgy = 402, iblock_x = 2, iblock_y = 2,
 LT = 20d0,40d0, RT = 40d0,40d0,
 LB = 20d0, 0d0, RB = 40d0, 1.7498d0 /  ! ***CHANGE BEND ANGLE HERE***

&boundaries_list
 bcL = 'none', bcR = 'absorbing', bcB = 'none', bcT = 'absorbing' /

!---ALL BLOCKS---

&fields_list
 problem = 'uniform', ! spatially uniform prestress in medium
 syy0 = -120d0, ! yy component of prestress (fault normal compression)
 sxy0 =   70d0, ! xy component of prestress (shear stress on fault)
 Psi = 40d0 ! angle of maximum compressive stress to fault (used to set sxx0)
 /

&material_list
 rho = 2.67d0, ! density
 cs = 3.464d0, ! S-wave speed
 cp = 6d0      ! P-wave speed
 /

&operator_list
 Cdiss = 0d0 / ! artificial dissipation to control numerical oscillations

!---IFACE1---

&interface_list
 coupling = 'locked', iblockm = 1, iblockp = 2,
 direction = 'x', mg = 1, pg = 401 /

!---IFACE2---

&interface_list
 coupling = 'friction', ! frictional interface
 iblockm = 1, iblockp = 3, ! between blocks 1 and 2
 direction = 'y', mg = 1, pg = 601 / ! with unit normal nominally in y direction

!---IFACE3---

&interface_list
 coupling = 'friction', iblockm = 2, iblockp = 4,
 direction = 'y', mg = 602, pg = 802 /

!---IFACE4---

&interface_list
 coupling = 'locked', iblockm = 3, iblockp = 4,
 direction = 'x', mg = 402, pg = 802 /

!---IFACE2and3---

&friction_list
 friction_law = 'SW', ! slip-weakening friction
 sw%fs = 0.677d0, ! static friction
 sw%fd = 0.525d0, ! dynamic friction
 sw%Dc = 0.4d0,   ! slip-weakening distance
 force = T, ! force rupture expansion to nucleate
 kn%fd = 0.525d0, ! dynamic friction (forced nucleation)
 kn%dfdx = 0.2d0, ! rate of increase of friction with x (forced nucleation)
 kn%vL = -1d0,    ! rupture velocity of left tip (forced nucleation)
 kn%vR =  1d0,    ! rupture velocity of right tip (forced nucleation)
 kn%xL =  0.76d0, ! initial position of left tip (forced nucleation)
 kn%xR = -0.76d0, ! initial position of right tip (forced nucleation)
 /

The output list below controls which fields will be output (written to
disk as binary data files) and how frequently. These are not namelists
and comments are not permitted. The list is organized into multiple
sections, such as
	  I
	  iface1
	  D V S N Psi
	  0d0 1d10 10
where "I" is an arbitrary name used to help you organize output, 
"iface1" is the location of the fields to be output, "D V S N"
is a list of the fields to be output (order does not matter), and
"0d0 1d10 1" saves data between times t=0 and t=10^10 (basically 
for all time) every 1 time steps.

!---begin:output_list---
I2
iface2
x y
0d0 0d0 1
I2
iface2
D V S N
0d0 1d10 1
I3
iface3
x y
0d0 0d0 1
I3
iface3
D V S N
0d0 1d10 1
B
body
x y
0d0 0d0 1
B
body
vx vy sxx sxy syy szz
0d0 1d10 10
!---end:output_list---

