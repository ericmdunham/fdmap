clear, clc
% NOTE RUN on matlab 2020 or older 
% for mazama load: module load matlab/R2020b

% Here we set up initial conditions to verify new reading of eta0 works
% This is a fun problem becasue when t = 0 eta =0
% So we can test old version and new verision by offsetting time t = .1*pi/omega
% We utalize (Lotto & Dunham, 2015) for verifcation



% paramaters
rho = 1; %g/cm^3
c0 = 1.500; % acoustic wave speed km/s
g = 9.8e-3; % gravity (km/s^2)
H = 5; %km


% STEP 3: evaluate dispersion relation to obtain omega = omega(k)
% Note in final result plots I plot the dispersion relation to see we are
% solving the surface gravity mode
    % Dispersion Relation
    % k* =   sqrt(k ^2   - omega^2/c0^2)
    % 1)- >  omega^2 = c0^2 k^2 - c0^2 k*^2
    % \omega  =  \sqrt{gk^* \tanh{(H k^*)}}.
    % 2)-> % \omega^2  =  gk^* tanh(H k^*)
    % Combine 1) and 2)
    % - > fun1 = 0 = c0^2 k^2 - c0^2 k*^2 - gk^* tanh(H k^*)
    % ** So for a given k
    % **  k* = fzero(fun1,[0,1])
    % ** solve for omega = sqrt(c0^2 k^2 - c0^2 k*^2)


k = pi/100; % 1/km
fun1 = @(kbar) c0^2*(k^2) - c0^2*kbar.^2 - g*kbar.*tanh(kbar.*H);
kbar = fzero(fun1,[0,k]); 
omega = sqrt(c0^2*k^2 - c0^2 *  kbar ^2);


% !!! If want eta0 = 0, p0=0, and vz0 = val, vx0 = val;
%offset = 0
% !!! If want eta0 = val, p0=val, and vz0 = 0, vx0 = 0;
offset = 0.5*pi/omega


x(:,1,1) = linspace(0,100,101);
y(1,:,1) = linspace(-5,0,16);
t(1,1,:) = linspace(0,2*pi/omega,1001)+offset;
% Solution From (Lotto & Dunham 2015)
p = sin(k*x).*sin(omega*t) .* (sinh(kbar*y) + g*kbar/omega^2 .*cosh(kbar*y));
vz= kbar/(rho*omega).*sin(k*x).*cos(omega*t).* (cosh(kbar*y)+g*kbar/omega^2 .*sinh(kbar*y));
vx = k/(rho*omega).*cos(k*x).*cos(omega*t).* (sinh(kbar*y)+g*kbar/omega^2 .*cosh(kbar*y));
eta = kbar/(rho*omega^2) .* sin(k*x) .* sin(omega *t);

format long
kbar/(rho*omega^2)

% Note the thicker lines are the start and end conditions
figure,
subplot(2,2,1), hold on
plot(x,permute(eta,[1,3,2]))
plot(x,permute(eta(:,1),[1,3,2]),'LineWidth',2)
plot(x,permute(eta(:,end),[1,3,2]),'--','LineWidth',2)
title('eta on surface')
subplot(2,2,2), hold on
plot(x,permute(p(:,end,:),[1,3,2]))
plot(x,permute(p(:,end,1),[1,3,2]),'LineWidth',2)
plot(x,permute(p(:,end,end),[1,3,2]),'--','LineWidth',2)
title('p on surface')
subplot(2,2,3), hold on
plot(x,permute(vz(:,end,:),[1,3,2]))
plot(x,permute(vz(:,end,1),[1,3,2]),'LineWidth',2)
plot(x,permute(vz(:,end,end),[1,3,2]),'--','LineWidth',2)
title('vz on surface')
subplot(2,2,4), hold on
plot(x,permute(vx(:,end,:),[1,3,2]))
plot(x,permute(vx(:,end,1),[1,3,2]),'LineWidth',2)
plot(x,permute(vx(:,end,end),[1,3,2]),'--','LineWidth',2)
title('vx on surface')
sgtitle('Fields through time')

p0 = p(:,:,1);
vz0 = vz(:,:,1);
vx0 = vx(:,:,1);
eta0 = eta(:,:,1);

pend = p(:,:,end);
vzend = vz(:,:,end);
vxend = vx(:,:,end);
etaend = eta(:,:,end);

figure,
subplot(4,2,1), hold on
plot(x,eta0)
title('eta at t=0 on surface')
subplot(4,2,3), hold on
plot(x,p0(:,end))
title('p0 at t=0 on surface')
subplot(4,2,5), hold on
plot(x,vz0(:,end))
title('vz0 at t=0 on surface')
subplot(4,2,7), hold on
plot(x,vx0(:,end))
title('vx0 at t=0 on surface')
subplot(4,2,4), hold on
plot(x,p0(:,1))
title('p0 at t=0 on floor')
subplot(4,2,6), hold on
plot(x,vz0(:,1))
title('vz0 at t=0 on floor')
subplot(4,2,8), hold on
plot(x,vx0(:,1))
title('vx0 at t=0 on floor')
sgtitle('Fields at the beginging of sims')

figure,
subplot(4,2,1), hold on
plot(x,etaend)
title('eta at t=end on surface')
subplot(4,2,3), hold on
plot(x,pend(:,end))
title('p0 at t=end on surface')
subplot(4,2,5), hold on
plot(x,vzend(:,end))
title('vz0 at t=end on surface')
subplot(4,2,7), hold on
plot(x,vxend(:,end))
title('vx0 at t=end on surface')
subplot(4,2,4), hold on
plot(x,pend(:,1))
title('p0 at t=end on floor')
subplot(4,2,6), hold on
plot(x,vzend(:,1))
title('vz0 at t=end on floor')
subplot(4,2,8), hold on
plot(x,vxend(:,1))
title('vx0 at t=end on floor')
sgtitle('Fields at the end of sims')

nx = length(x);
ny = length(y);
sxz0 = zeros(nx,ny);

% Write FDMap Init Conditions 
% This is with current implementation 7/22/2022 
WRITEFILE = 0; % 0 off, 1 on 
if WRITEFILE
    outfile = 'water_toffset.initCond'
    endian = 'n'; % native endian
    prec = 'real*8'; % double precision
    % open file
    [fid,m] = fopen(outfile,'w',endian);
    if fid==-1,disp(m),return,end
    % write to disk
    fwrite(fid,vx0,prec); % nonzero forcing
    fwrite(fid,vz0,prec); % must write these, too
    fwrite(fid,-p0,prec); % must write these, too
    fwrite(fid,sxz0,prec); % must write these, too
    fwrite(fid,-p0,prec); % must write these, toos
    fwrite(fid,-p0,prec); % must write these, too 
   fclose(fid);
end

if offset > 0
if WRITEFILE
    outfile = 'water_eta0_toffset.initCond'
    endian = 'n'; % native endian
    prec = 'real*8'; % double precision
    % open file
    [fid,m] = fopen(outfile,'w',endian);
    if fid==-1,disp(m),return,end
    % write to disk
    fwrite(fid,eta0,prec); % nonzero forcing
end
end

