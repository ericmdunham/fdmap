
% Visualize Results from the water_init cond problem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear, clc, close all
PATH= ['~/fdmap/data/'];
name= 'water_t0_initCond';
A = init(name,PATH);
X=loadfast(A,'S_x_x'); % x coordinate of both ice and open water parts of domain
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  vx_sim=loadfast(A,'S_x_vx'); % vert vel of open water
  vy_sim=loadfast(A,'S_x_vy'); % vert vel of open water
  p_sim=loadfast(A,'S_x_sxx'); % vert vel of open water
  t_sim=vx_sim.t;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Exact solution
% paramaters
rho = 1; %g/cm^3
c0 = 1.500; % acoustic wave speed km/s
g = 9.8e-3; % gravity (km/s^2)
H = 5; %km
offset = 0;

% STEP 3: evaluate dispersion relation to obtain omega = omega(k)
% Note in final result plots I plot the dispersion relation to see we are
% solving the surface gravity mode
    % Dispersion Relation
    % k* =   sqrt(k ^2   - omega^2/c0^2)
    % 1)- >  omega^2 = c0^2 k^2 - c0^2 k*^2
    % \omega  =  \sqrt{gk^* \tanh{(H k^*)}}.
    % 2)-> % \omega^2  =  gk^* tanh(H k^*)
    % Combine 1) and 2)
    % - > fun1 = 0 = c0^2 k^2 - c0^2 k*^2 - gk^* tanh(H k^*)
    % ** So for a given k
    % **  k* = fzero(fun1,[0,1])
    % ** solve for omega = sqrt(c0^2 k^2 - c0^2 k*^2)


k = pi/100; % 1/km
fun1 = @(kbar) c0^2*(k^2) - c0^2*kbar.^2 - g*kbar.*tanh(kbar.*H);
kbar = fzero(fun1,[0,k]); 
omega = sqrt(c0^2*k^2 - c0^2 *  kbar ^2);

x(:,1,1) = linspace(0,100,101);
y(1,:,1) = linspace(-5,0,26);
t(1,1,:) = t_sim;
% Solution From (Lotto & Dunham 2015)
p_exact = sin(k*x).*sin(omega*t) .* (sinh(kbar*y) + g*kbar/omega^2 .*cosh(kbar*y));
vz_exact= kbar/(rho*omega).*sin(k*x).*cos(omega*t).* (cosh(kbar*y)+g*kbar/omega^2 .*sinh(kbar*y));
vx_exact = k/(rho*omega).*cos(k*x).*cos(omega*t).* (sinh(kbar*y)+g*kbar/omega^2 .*cosh(kbar*y));
eta_exact = kbar/(rho*omega^2) .* sin(k*x) .* sin(omega *t);

temp_p = permute(p_exact(:,end,:),[1,3,2]);
figure, pcolored(x,t,-p_sim.sxx)
figure, pcolored(x,t,temp_p)

temp_vz = permute(vz_exact(:,end,:),[1,3,2]);
figure, pcolored(x,t,vy_sim.vy)
figure, pcolored(x,t,temp_vz)

temp_vx = permute(vx_exact(:,end,:),[1,3,2]);
figure, pcolored(x,t,vx_sim.vx)
figure, pcolored(x,t,temp_vx)




