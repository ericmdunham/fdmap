% DMG aseismic slip study
% elasticity test
% script to calculate domain geometry

runSetUp = false;

if runSetUp

    dip = 70;
    
    Z = [0 1.52 2.51 3.25 5.2 6.22 30]';
    nZ = length(Z)-1; % number of layers = nyblocks
    
    mu = [20 16 13 12 24 30];
    nu = [0.27 0.26 0.25 0.24 0.28 0.32];
    rho = 2.7;
    cs = sqrt(mu/rho);
    M = 2*mu.*(1-nu)./(1-2*nu); cp = sqrt(M/rho);
    
    % mode 3 effective properties
    
    n = 2; % DMG layer number
    mu3 = mu(n)/(1-nu(n)); % effective modulus
    cs3 = sqrt(mu3/rho) % effective S-wave speed
    
    X = Z/tand(dip); % layer horizontal positions
    
    W = 30; % domain half width
    
    [X-W X X+W] % x coordinates of block corners
    [Z Z Z] % y coordinates of block corners

end
    
setupMode3 = false;
setupMode2 = false;

% mode 3
if setupMode3
    nPoints = 1201;
    zfault = linspace(0,Z(end),nPoints); % distance down dip
    zmid = 0.5*(Z(n)+Z(n+1))/sind(dip); zwidth = 0.5*(Z(n+1)-Z(n))/sind(dip);
    slip = real((1-(zfault-zmid).^2/zwidth^2).^(3/2)); slip = slip/max(slip);
    slip(slip==0)=1e-6;
    slip = fliplr(slip);
    trup = zeros(1,nPoints);
    Vpeak = ones(1,nPoints);
    fdmap_write_pseudodynamic('DMGmode3slip.pd',slip,trup,Vpeak)
end

% mode 2
if setupMode2
    h = 0.025*sind(dip);
    nPoints = round(diff(Z)/h)+1
    xfault = linspace(X(n),X(n+1),nPoints(n));
    xmid = 0.5*(X(n)+X(n+1)); xwidth = 0.5*(X(n+1)-X(n));
    slip = real((1-(xfault-xmid).^2/xwidth^2).^(3/2)); slip = slip/max(slip);
    slip(slip==0)=1e-6;
    slip = fliplr(slip);
    trup = zeros(1,nPoints(n));
    Vpeak = ones(1,nPoints(n));
    fdmap_write_pseudodynamic('DMGmode2slip.pd',slip,trup,Vpeak)
end

% plots

makePlots = true;
if makePlots

    A = init('DMGslip3','~/fdmap/data');
    X=loadfast(A,'I_x');Y=loadfast(A,'I_y');Z=sqrt(X.^2+Y.^2);
    S=loadfast(A,'I_S');D=loadfast(A,'I_D');
    Zmode3=Z;Smode3=S.S(:,end);Dmode3=D.D(:,end);

    A = init('DMGslip2','~/fdmap/data');
    X=loadfast(A,'I2_x');Y=loadfast(A,'I2_y');Z2=sqrt(X.^2+Y.^2);
    S2=loadfast(A,'I2_S');D2=loadfast(A,'I2_D');N2=loadfast(A,'I2_N');
    X=loadfast(A,'I3_x');Y=loadfast(A,'I3_y');Z3=sqrt(X.^2+Y.^2);
    S3=loadfast(A,'I3_S');D3=loadfast(A,'I3_D');N3=loadfast(A,'I3_N');
    X=loadfast(A,'I4_x');Y=loadfast(A,'I4_y');Z4=sqrt(X.^2+Y.^2);
    S4=loadfast(A,'I4_S');D4=loadfast(A,'I4_D');N4=loadfast(A,'I4_N');
    X=loadfast(A,'I5_x');Y=loadfast(A,'I5_y');Z5=sqrt(X.^2+Y.^2);
    S5=loadfast(A,'I5_S');D5=loadfast(A,'I5_D');N5=loadfast(A,'I5_N');
    X=loadfast(A,'I6_x');Y=loadfast(A,'I6_y');Z6=sqrt(X.^2+Y.^2);
    S6=loadfast(A,'I6_S');D6=loadfast(A,'I6_D');N6=loadfast(A,'I6_N');
    Zmode2 = [Z2; Z3; Z4; Z5; Z6];
    Smode2 = [S2.S(:,end); S3.S(:,end); S4.S(:,end); S5.S(:,end); S6.S(:,end)];
    Dmode2 = [D2.D(:,end); D3.D(:,end); D4.D(:,end); D5.D(:,end); D6.D(:,end)];
    Nmode2 = [N2.N(:,end); N3.N(:,end); N4.N(:,end); N5.N(:,end); N6.N(:,end)];

    scale = 0.2;
    subplot(2,1,1)
    plot(Zmode3,Dmode3)
    %plot(Zmode3,Dmode3*scale,Zmode2,Dmode2*scale)
    %legend('antiplane shear','plane strain','location','southeast')
    xlim([1 3.5])
    xlabel('distance down dip (km)')
    ylabel('slip (m)')
    subplot(2,1,2)
    plot(Zmode3,Smode3*scale,Zmode2,Smode2*scale,Zmode2,Nmode2*scale)
    xlabel('distance down dip (km)')
    ylabel('stress change (MPa)')
    legend('\Delta\tau, antiplane shear','\Delta\tau, plane strain','\Delta\sigma, plane strain','location','southeast')
    xlim([1 3.5])

    [~,ind,~] = unique(Zmode2);
    Smode2interp = interp1(Zmode2(ind),Smode2(ind),Zmode3);
    indx = find(1<=Zmode3&Zmode3<=3.5);
    errS = scale*norm(Smode2interp(indx)-Smode3(indx))/sqrt(length(indx)) % rms error
    
end