#!/bin/bash
#
#SBATCH --partition=serc
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
#SBATCH --output=gp229_%j.out
#SBATCH --error=gp229_%j.err
#SBATCH --constraint=CPU_GEN:RME
#SBATCH --time=01:00:00

#run on Rome node (make sure to use constraint=CPU_GEN:RME above)
. /home/groups/s-ees/share/cees/spack_cees/scripts/cees_sw_setup-beta.sh
module purge
module load gcc-cees-beta/
module load openmpi-cees-beta/
module load openblas-cees-beta/

date
srun fdmap gp229/gp229sw3ex1.in
date

