#!/bin/bash
#
#SBATCH --partition=serc
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --output=fdmap_compile_%j.out
#SBATCH --error=fdmap_compile_%j.err
#SBATCH --constraint=CPU_GEN:RME
#SBATCH --time=00:15:00
#
# compile_fdmap-sherlock.sh
# principal authors: Mark R. Yoder, Stanford Research Computing
#  Randy "RC" White, Stanford Research Computing
#
#run on Rome node (make sure to use constraint=CPU_GEN:RME above)
. /home/groups/s-ees/share/cees/spack_cees/scripts/cees_sw_setup-beta.sh
module purge
module load gcc-cees-beta/
module load openmpi-cees-beta/
module load openblas-cees-beta/
#
if [[ -z ${SLURM_CPUS_PER_TASK} ]]; then
    NCPUS=1
else
    NCPUS=${SLURM_CPUS_PER_TASK}
fi
#
#echo "modules: "
#module list
#
export FC=mpifort
export CC=mpicc
export CXX=mpicxx
export MPIFC=mpifort
export MPICC=mpicc
export MPICXX=mpicxx
#
export LD="${MPIFC}"
#
# debug:
export FCFLAGS="-fdefault-real-8 -fdefault-double-8 -g -Wall -Wextra -Wconversion -fbounds-check -fbacktrace -fimplicit-none -ffpe-summary=all -ffpe-trap=invalid,zero,overflow -std=f2003 -Wno-compare-reals"
export LDFLAGS="${FCFLAGS}"
#
# production:
#export FCFLAGS="-fdefault-real-8 -fdefault-double-8 -O5 -Wuninitialized"
#export LDFLAGS="-O5 -Wuninitialized"
#
#
export INCL="-fall-intrinsics "
LIBS="-Wl,-rpath -Wl,--enable-new-dtags `pkg-config --libs-only-L ompi-fort` `pkg-config --libs-only-l ompi-fort` "
#
# gcc/blas:
export LIBS="${LIBS} -lopenblas -BStatic"
#
make -f Makefile.noflags clean
make -f Makefile.noflags -j ${NCPUS}

